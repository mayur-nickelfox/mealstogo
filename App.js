/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Platform,
} from 'react-native';



function App() {
  const isAndroid = Platform.OS === 'android';
  return (
    <>
      <SafeAreaView style={{ flex: 1 }}>
        <View>
          <Text>Hello World</Text>
        </View>
      </SafeAreaView>
      <StatusBar style="auto" />
    </>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    backgroundColor: 'red',
    paddingHorizontal: 24,
  },
});

export default App;
